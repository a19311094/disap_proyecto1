package aimlion.com.proyecto1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    lateinit var tipo : EditText
    lateinit var number1 : EditText
    lateinit var calcular : Button
    lateinit var resultado : TextView

    var calculo : Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tipo = findViewById(R.id.et_type)
        number1 = findViewById(R.id.et_metro)
        calcular = findViewById(R.id.btn_equal)
        resultado = findViewById(R.id.tv_result)

        calcular.setOnClickListener {

            Toast.makeText(this,"Calculando . . .", Toast.LENGTH_SHORT).show()
            calcularCosto()
        }

    }

    fun calcularCosto(){

        var normal : Int = 0
        var tierra : Int = 0
        var resulta : Int = 0
        var num1 : Int = number1.text.toString().toInt()
        var Tcable : String = tipo.text.toString().toString()

        if( Tcable=="Normal"){
            normal = 125
            resulta = num1 * normal
        }
        if( Tcable=="Tierra"){
            tierra = 145
            resulta = num1 * tierra
        }

        resultado.text = resulta.toString()
    }
}